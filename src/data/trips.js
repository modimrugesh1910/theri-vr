const trips = [
  {
    id: 1,
    title: 'Incredible India',
    date: 'September 18-20, 2016',
    category: 'india',
    locations: [
      {
        name: 'Taj Mahal, Agra, Uttar Pradesh',
        dec: [27.173891, 78.042068],
        gps: {
          lat: {
            deg: 27,
            min: 10
          },
          long: {
            deg: 78,
            min: 2
          }
        }
      },
      {
        name: 'Red Fort, Lal Qila, Delhi',
        dec: [28.656473, 77.242943],
        gps: {
          lat: {
            deg: 28,
            min: 39
          },
          long: {
            deg: 77,
            min: 14
          }
        }
      },
      {
        name: 'Golden Temple',
        dec: [31.633980, 74.872261],
        gps: {
          lat: {
            deg: 31,
            min: 38
          },
          long: {
            deg: 74,
            min: 52
          }
        }
      },
      {
        name: 'Charminar, Hyderabad',
        dec: [17.361431, 78.474533],
        gps: {
          lat: {
            deg: 17,
            min: 21
          },
          long: {
            deg: 78,
            min: 28
          }
        }
      }
    ],
    mapZoom: 5,
    excerpt: '',
    summaryPs: [
      'Taj Mahal is a monument located in Agra in India, constructed between 1631 and 1653 by a workforce of more than 20,000. The Mughal Emperor Shah Jahan commissioned its construction as a mausoleum for his favorite wife Mumtaz Mahal. ... While the white domed marble mausoleum is the most familiar part of the monument.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571815541/sabre/famous%20place/tajmahal-unsplash_gnifgh.jpg',
        w: 1800,
        h: 1260
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571815531/sabre/famous%20place/indian-red-fort_lt49gc.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571815545/sabre/famous%20place/golden-temple_bmfauy.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571815547/sabre/famous%20place/charminar-hydrabad_worb29.jpg',
          w: 1680,
          h: 1200
        }
      ]
    }
  },
  {
    id: 2,
    title: 'Rajasthan',
    date: 'July 18-29, 2018',
    category: 'rajasthan',
    locations: [
      {
        name: 'Jaipur',
        dec: [26.9220704,75.778885],
        gps: {
          lat: {
            deg: 26,
            min: 55
          },
          long: {
            deg: 75,
            min: 46
          }
        }
      },
      {
        name: 'Jodhpur',
        dec: [26.263863,73.008957],
        gps: {
          lat: {
            deg: 26,
            min: 15
          },
          long: {
            deg: 73,
            min: 0
          }
        }
      },
      {
        name: 'Udaipur',
        dec: [24.571270,73.691544],
        gps: {
          lat: {
            deg: 24,
            min: 34
          },
          long: {
            deg: 73,
            min: 41
          }
        }
      },
      {
        name: 'Jaisalmer',
        dec: [26.911661,70.922928],
        gps: {
          lat: {
            deg: 26,
            min: 54
          },
          long: {
            deg: 70,
            min: 55
          }
        }
      }
    ],
    mapZoom: 6,
    excerpt: 'Padharo mahare desh',
    summaryPs: [
      'Rajasthan is beautiful and largest state of India. Jaipur  (Pink City) is capital of Rajasthan. People speak here Rajasthani Language.  The main geographic features of Rajasthan are the Thar Desert and the Aravalli Range, Mount Abu, Raisina Hill, Thar, Luni River and may more.',
      'The palaces of Jaipur, lakes of Udaipur, and desert forts of Jodhpur, Bikaner & Jaisalmer rank among the most preferred destinations in India for many tourists both Indian and foreign.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_600,w_1000/v1571809416/sabre/rajasthan/udaipur-hotel_acr8ww.jpg',
        w: 1000,
        h: 600
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571809421/sabre/rajasthan/hava_mahal_ocv28g.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571809411/sabre/rajasthan/lake_palace_dkgv8s.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571809417/sabre/rajasthan/Jodhpur_City_rc3ido.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571809419/sabre/rajasthan/jaisalamar_v8mcb1.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571809423/sabre/rajasthan/fort_sboaiz.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571811773/sabre/rajasthan/rajasthan-desert_vvdjx5.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1680,w_1260/v1571811775/sabre/rajasthan/Jantar_Mantar_Jaipur_xylpnm.jpg',
          w: 1680,
          h: 1260
        },
      ]
    }
  },
  {
    id: 3,
    title: 'Their survival is in your hands',
    date: 'May 30, 2017',
    category: 'himalaya',
    locations: [
      {
        name: 'Himalaya Mountain',
        dec: [28.5983,83.9311],
        gps: {
          lat: {
            deg: 28,
            min: 35
          },
          long: {
            deg: 83,
            min: 55
          }
        }
      }
    ],
    mapZoom: 8,
    excerpt: 'Every so often it becomes apparent that the world can be pretty inspiring, even in your own backyard.',
    summaryPs: [
      "Himalaya is a mountain range in Asia, separating the Indian subcontinent from the Tibetan Plateau. ... The Himalaya stretches across five nations, Bhutan, China, India, Nepal, and Pakistan. It is the source of three of the world's major river systems, the Indus Basin, the Ganga-Brahmaputra Basin and the Yangtze Basin."
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,w_1680/v1571736508/sabre/himalaya/himalaya-1.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1571736493/sabre/himalaya/himalaya-2.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1571736480/sabre/himalaya/himalaya-3.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1120,w_1680/v1571736461/sabre/himalaya/himalaya-4.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_800/v1571736491/sabre/himalaya/himalaya-5.jpg',
          w: 799,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1260,w_900/v1571736479/sabre/himalaya/himalaya-6.jpg',
          w: 900,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571736488/sabre/himalaya/himalaya-7.jpg',
          w: 1680,
          h: 1116
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571738354/sabre/himalaya/himalaya-9.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571738358/sabre/himalaya/himalaya-10.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1400,w_1000/v1571738353/sabre/himalaya/himalaya-8.jpg',
          w: 1000,
          h: 1400
        },
      ]
    }
  },
  {
    id: 4,
    title: 'A Perfect Holiday Destination',
    date: 'March 18-19, 2019',
    category: 'goa',
    locations: [
      {
        name: 'Panjim, Goa',
        dec: [15.496777,73.827827],
        gps: {
          lat: {
            deg: 15,
            min: 29
          },
          long: {
            deg: 73,
            min: 49
          }
        }
      }
    ],
    mapZoom: 5,
    excerpt: 'Best part of the day is the sunrise and the rainbow while the stars and the moon in the naked sky are the favorite part of our night.',
    summaryPs: [
      'Goa is a coastal region located in the western part of India famous as most popular tourist destination.',
      'Goa is one of the most important tourist spots in the country because of its beauty and culture. If you love beach and adventure activities, this is the best place to enjoy.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571813788/sabre/goa/GOA-beach_cwerye.jpg',
        w: 1680,
        h: 1200
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571813788/sabre/goa/goa-church_md8tmq.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571813790/sabre/goa/goa-gJhev0YgUcE-unsplash_vufv5q.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1200,w_1680/v1571813791/sabre/goa/goa-sunset_erwozf.jpg',
          w: 1680,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/c_scale,h_1180,w_1500/v1571813862/sabre/goa/Querim_Beach_Goa_suih0q.jpg',
          w: 1180,
          h: 1500
        }
      ]
    }
  },
  {
    id: 5,
    title: 'Big Sur',
    date: 'August 5, 2016',
    category: 'north-america',
    locations: [
      {
        name: 'Big Sur, California',
        dec: [36.3318028,-121.9582603],
        gps: {
          lat: {
            deg: 36,
            min: 19
          },
          long: {
            deg: -121,
            min: 57
          }
        }
      },
    ],
    mapZoom: 7,
    excerpt: 'Always been a pretty big fan of the pacific coast drives. In Big Sur, it doesn\'t get much better.',
    summaryPs: [
      'Big Sur is a rugged and mountainous section of the Central Coast of California between Carmel Highlands and San Simeon, where the Santa Lucia Mountains rise abruptly from the Pacific Ocean. It is frequently praised for its dramatic scenery..',
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939107/sabre/north-america-1/north-america-1_bcn8wo.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939088/sabre/north-america-1/north-america-2_hoapsa.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939094/sabre/north-america-1/north-america-3_rdvmjm.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939096/sabre/north-america-1/north-america-4_oxa5a4.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939462/sabre/north-america-1/north-america-5_bkxdav.jpg',
          w: 800,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939461/sabre/north-america-1/north-america-6_lew1yd.jpg',
          w: 801,
          h: 1200
        },
      ]
    }
  },
  {
    id: 6,
    title: 'The Park',
    date: 'August 16, 2016',
    category: 'north-america',
    locations: [
      {
        name: 'Yosemite',
        dec: [37.8651053,-119.5405181],
        gps: {
          lat: {
            deg: 37,
            min: 51
          },
          long: {
            deg: -119,
            min: 32
          }
        }
      },
    ],
    mapZoom: 6,
    excerpt: 'Used to summer camp here as a kid, revisiting it as an adult doesn\'t dimish the scale.',
    summaryPs: [
      'Designated a World Heritage site in 1984, Yosemite is internationally recognized for its granite cliffs, waterfalls, clear streams, giant sequoia groves, lakes, mountains, meadows, glaciers, and biological diversity. Almost 95% of the park is designated wilderness.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939621/sabre/north-america-2/north-america-1_ieqm7w.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939608/sabre/north-america-2/north-america-2_wxqsup.jpg',
          w: 1680,
          h: 1057
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939631/sabre/north-america-2/north-america-3_o7i4a2.jpg',
          w: 1680,
          h: 945
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939635/sabre/north-america-2/north-america-4_trdy2c.jpg',
          w: 1680,
          h: 1117
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939616/sabre/north-america-2/north-america-5_neqs55.jpg',
          w: 892,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939649/sabre/north-america-2/north-america-6_jucqg7.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939648/sabre/north-america-2/north-america-7_wtgoru.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1571939637/sabre/north-america-2/north-america-8_gzfcqr.jpg',
          w: 1680,
          h: 1120
        },
      ]
    }
  },
  {
    id: 7,
    title: 'Jack Frost',
    date: 'Jan 6-8, 2016',
    category: 'north-america',
    locations: [
      {
        name: 'Sundance, Utah',
        dec: [40.3934203,-111.6062805],
        gps: {
          lat: {
            deg: 40,
            min: 23
          },
          long: {
            deg: -111,
            min: 36
          }
        }
      }
    ],
    mapZoom: 6,
    excerpt: 'A suprisingly warm touch was felt amongst some of the coldest skiing I\'ve ever done.',
    summaryPs: [
      'Sundance Ski Resort in Utah provides an ambiance of a rustic but elegant resort of the past without the glitz and hyper energy of some of the modern mega-resorts. The low speed lifts, the eco-friendly mantra and natural materials used in the buildings blend with nature and the mountains surrounding the resort.'
    ],
    images: {
      featured: {
        src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181892/sabre/north-america-3/north-america-1_wf7uod.jpg',
        w: 1680,
        h: 1120
      },
      others: [
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181893/sabre/north-america-3/north-america-2_vjdp9v.jpg',
          w: 1680,
          h: 1120
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181881/sabre/north-america-3/north-america-3_vkhmlc.jpg',
          w: 900,
          h: 1200
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181891/sabre/north-america-3/north-america-4_j7rzxb.jpg',
          w: 1680,
          h: 894
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181892/sabre/north-america-3/north-america-5_z8ozux.jpg',
          w: 1680,
          h: 1113
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181898/sabre/north-america-3/north-america-6_bqtjff.jpg',
          w: 1680,
          h: 1260
        },
        {
          src: 'https://res.cloudinary.com/dqqxkw1g1/image/upload/v1572181898/sabre/north-america-3/north-america-7_idfplp.jpg',
          w: 1680,
          h: 1121
        },
      ]
    }
  }
 
];

export default trips;
