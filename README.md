# Theri 

We are traveling at least once during the year. When we view the memories of travel to family members, the photographs are in 2D format, they just only see that memories. In this 21st century, using technology we can see it in VR board and feel like we are in that places.
Our Objective is to build a feature which displays unsplash free photos collection in a VR mode.  

There are a ton of really interesting developments happening in the VR space right now and some are even [focused on the web](http://www.vrfavs.com/#WebVR), which is awesome! 

Currently, in the app when the "simulate VR" toggle is active, the scene renders full screen and utilizes mouse movement to simulate the head tracking of a VR headset.


### Tools used

A list of all the tools can be found in the [`package.json`](package.json), but here are some of the bigger UI focused tools:

* [**React**](https://facebook.github.io/react/) - view layer.
* [**React Photoswipe**](https://github.com/vn38minhtran/react-photoswipe) - React implementation of the popular [Photoswipe](http://photoswipe.com/) photo gallery; 
* [**React Leaflet**](https://github.com/PaulLeCam/react-leaflet) - React implementation of the [Leaflet](http://leafletjs.com/), used in conjunction with a custom [Mapbox](https://www.mapbox.com/) tileset.
* [**Express.js**](http://expressjs.com) - render app.
* [**Unsplash**](https://unsplash.com/) - good quality free photos.

### run app in local
1. Download file/ clone the app code from bitbucket.
2. Unzip the code
3. Write npm install to install dependencies
4. Run the app using npm start
5. The app is running on http://localhost:3000 in the browser.